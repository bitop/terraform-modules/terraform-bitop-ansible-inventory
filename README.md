# terraform-bitop-ansible-inventory

A Terraform module that will render groups of server IP addresses into an Ansible inventory file and output it to the local filesystem at a specified location.

The module will also take arbitrary secrets (certificates, tokens, keys, etc.) and write them to files in a subdirectory of the output path.

## Usage

Example:

```hcl
provider "local" {
  version = "~> 1.3"
}

module "inventory_production" {
  source  = "gendall/ansible-inventory/local"
  servers = {     
    manager = ["1.2.3.4"]
    worker = ["11.22.33.44", "55.66.77.88"]
  }
  secrets = {
    tls_key  = "-----BEGIN RSA PRIVATE KEY----- MIIEow..."
    tls_cert = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD..."
  }
  output  = "inventory/production"
}
```

Example Using BitOP Terraform VM:

```hcl
provider "vsphere" {
  user           = 'administrator@vsphere.local'
  password       = 'P@ssW0rd'
  vsphere_server = 'vsphere.data.net'

  # if you have a self-signed cert
  allow_unverified_ssl = true
}

module "Docker" {
  source            = "git::https://gitlab.com/bitop/terraform-modules/terraform-bitop-vm.git?ref=v1.0.0"

  # vsphere data
  dc                = "Datacenter"
  datastore         = "Datastore"
  resource_pool     = "esxi-cluster/Resources"
  vm_template       = "ubuntu-20.04.1"

  # VM information
  instances         = 1
  vm_name           = "Linux-Server"
  vm_folder         = "DevOps"
  network           = {
    "vlan21" = ["","",""]
  }
  vm_gateway        = "192.168.1.1"
}

module "inventory_production" {
  source    = "git::https://gitlab.com/bitop/terraform-modules/terraform-bitop-ansible-inventory.git?ref=v1.0.0"
  servers   = {     
    docker  = module.Docker.Linux-ip
  }
  
  output    = "inventory/production"

}

```
