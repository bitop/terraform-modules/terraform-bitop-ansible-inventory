%{ for name, ips in servers ~}
[${name}]

%{ for ip in ips ~}
${ip}
%{ endfor ~}

%{ endfor ~}

%{ if hostkey}
[all:vars]
ansible_ssh_common_args='-o StrictHostKeyChecking=no'
%{ endif }
